from django.urls import re_path
from .api.views import layer_commit_vector_view

BASE_URLS = 'editing_iim'

urlpatterns = [
    re_path(r'^api/(?P<mode_call>editing|commit|unlock)/(?P<project_type>[-_\w\d]+)/(?P<project_id>[0-9]+)/'
        r'(?P<layer_name>[-_\w\d]+)/$',
        layer_commit_vector_view, name='editing-iim-commit-vector-api')
]


