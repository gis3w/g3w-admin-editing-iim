# -*- coding: utf-8 -*-

from django.conf import settings
from django.dispatch import receiver
from django.template import loader, TemplateDoesNotExist
from django.core.mail import send_mail
from django.urls import reverse
from rest_framework.exceptions import ValidationError
from usersmanage.models import User
from usersmanage.utils import userHasGroups
from core.signals import pre_save_maplayer, post_save_maplayer, post_create_maplayerattributes
from usersmanage.models import User, Group as AuthGroup
from editing.models import EDITING_POST_DATA_UPDATED, EDITING_POST_DATA_ADDED
from signaler_iim.utils import get_layer_orignames, get_map_link, propagate_state
from signaler_iim.models import SIIMSignalerProjects, SIIM_SIGNAL_TYPES, SlsSegnalazioni, Stati, SIIMSignalNote
import copy


import logging

logger = logging.getLogger('signaler_iim')


@receiver(pre_save_maplayer)
def validate_signal_layer(sender, **kwargs):
    """Checks signal layer constraints by users, add user pk to data
    sender: BaseEditingVectorOnModelApiView instance
    kwargs: ["layer_metadata", "mode", "data", "user"]
    """

    mode = kwargs['mode']
    if mode not in (EDITING_POST_DATA_UPDATED, EDITING_POST_DATA_ADDED):
        return

    signal_type = SIIMSignalerProjects.get_type(sender.layer.project)
    if signal_type not in SIIM_SIGNAL_TYPES.keys():
        return

    # Check if layer
    layers = get_layer_orignames(signal_type, sender.layer.project, '_')

    # Only if layer is a signaler iim layer from settings and mode is insert
    if sender.layer.qgs_layer_id in layers.values():

        # Validation based on state passage
        user = kwargs['user']

        # Check for every role type only for signal layer
        if sender.layer.origname == SIIM_SIGNAL_TYPES[signal_type]['signaler_layer_origname']:
            for role in settings.SIIM_USERS_GROUP.values():
                if role in settings.SIIM_SIGNAL_EDITING_STATE and \
                        not kwargs['data']['feature']['properties'][SIIM_SIGNAL_TYPES[signal_type]['father_field']]:
                    if userHasGroups(user, [role]) and \
                             kwargs['data']['feature']['properties'][settings.SIIM_SIGNAL_STATE_FIELD] not in \
                             settings.SIIM_SIGNAL_EDITING_CHANGE_STATE[role]:

                        raise ValidationError({
                            kwargs['layer_metadata'].client_var: {
                                mode: {
                                    'id': kwargs['data']['feature']['id'],
                                    'fields': f"Sorry you don't have permission to change to "
                                              f"state {kwargs['data']['feature']['properties'][settings.SIIM_SIGNAL_STATE_FIELD]}"
                                }
                            }
                        })



        # ADD data to feature before so save it
        if kwargs['mode'] == EDITING_POST_DATA_ADDED:
            try:
                # For signal
                if layers[SIIM_SIGNAL_TYPES[signal_type]['signaler_layer_origname']] == sender.layer.qgs_layer_id:

                    # Add user id to data
                    kwargs['data']['feature']['properties']['user'] = kwargs['user'].pk
                    kwargs['data']['feature']['properties']['capit_porto'] = user.siimuserextradata.cp

                # For features layer
                if 'signal_id' in kwargs['data']['feature']['properties']:
                    signal_id = kwargs['data']['feature']['properties']['signal_id']
                    signal = SIIM_SIGNAL_TYPES[signal_type]['model'].objects.get(pk=signal_id)
                    kwargs['data']['feature']['properties']['capit_porto'] = signal.capit_porto
                    kwargs['data']['feature']['properties']['user'] = signal.user

                # For vertex layer
                if 'feature_id' in kwargs['data']['feature']['properties']:
                    feature_id = kwargs['data']['feature']['properties']['feature_id']
                    feature = SIIM_SIGNAL_TYPES[signal_type]['geo_model'].objects.get(pk=feature_id)
                    if feature.capit_porto:
                       capit_porto_id = feature.capit_porto
                    else:

                        # Try to get from signal
                        capit_porto_id = SIIM_SIGNAL_TYPES[signal_type]['model'].objects.get(pk=feature.signal_id).capit_porto

                    kwargs['data']['feature']['properties']['capit_porto'] = capit_porto_id
                    kwargs['data']['feature']['properties']['user'] = feature.user


            except Exception as e:
                logger.error(f'Error in adding capito_porto value to layer: {sender.layer}: {e} '
                             f'on signal type {signal_type}')

        # Save Note
        if SIIM_SIGNAL_TYPES[signal_type]['note_field'] in kwargs['data']['feature']['properties']:
            if kwargs['data']['feature']['properties'][SIIM_SIGNAL_TYPES[signal_type]['note_field']]:
                sender.siim_note = copy.copy(kwargs['data']['feature']['properties'][SIIM_SIGNAL_TYPES[signal_type]['note_field']])
                kwargs['data']['feature']['properties'][SIIM_SIGNAL_TYPES[signal_type]['note_field']] = ''



@receiver(post_save_maplayer)
def send_email(sender, **kwargs):
    """ Send email on passage state """

    mode = kwargs['mode']
    if mode not in (EDITING_POST_DATA_UPDATED, EDITING_POST_DATA_ADDED):
        return

    signal_type = SIIMSignalerProjects.get_type(sender.layer.project)
    if signal_type not in SIIM_SIGNAL_TYPES.keys():
        return

    # Only for signaler_layer_origname
    layers = get_layer_orignames(signal_type, sender.layer.project, '_')

    if sender.layer.qgs_layer_id == layers[SIIM_SIGNAL_TYPES[signal_type]['signaler_layer_origname']]:

        # Only ancestor signal
        if kwargs['data']['feature']['properties'][SIIM_SIGNAL_TYPES[signal_type]['father_field']]:
            return

        if kwargs['original_feature']:
            from_state = kwargs['original_feature'].attribute('passaggio')
            to_state = kwargs['data']['feature']['properties']['passaggio']
            state_passage = f"{from_state}_{to_state}"
        else:
            from_state = None
            to_state = state_passage = kwargs['data']['feature']['properties']['passaggio']

        state_to = Stati.objects.get(value=kwargs['data']['feature']['properties']['passaggio'])

        signal_id = kwargs['to_res']['id'] if kwargs['to_res'] else kwargs['data']['feature']['id']

        # SAVE note history table
        # ===================================================
        if hasattr(sender, 'siim_note'):
            SIIMSignalNote.objects.create(
                type=signal_type,
                user=sender.request.user,
                signal_id=signal_id,
                note=sender.siim_note,
                from_state=from_state,
                to_state=to_state
            )

        # PROPAGATE STATE
        # ====================================================
        propagate_state(SIIM_SIGNAL_TYPES[signal_type]['model'].objects.get(pk=signal_id), signal_type)


        try:

            context = {
                'signal_type': signal_type,
                'reporter': User.objects.get(id=kwargs['data']['feature']['properties']['user']),
                'data': kwargs['data']['feature']['properties'],
                'map_link': get_map_link(signal_type, signal_id),
                'signals_link': reverse('ssim-signals-list-by-type', args=[signal_type]),
                'state_to': state_to
            }

            body_msg_template = loader.get_template(f"signaler_iim/email/{state_passage}.html")
            body_msg = body_msg_template.render(context)

            # Email address recovery
            to = None
            users = None
            if state_passage in ('1IL_2DV', '2DV'):

                # Get every supervisor by capit_porto
                users = AuthGroup.objects.get(name=settings.SIIM_USERS_GROUP['ccp_supervisor']).\
                    user_set.filter(siimuserextradata__cp=kwargs['data']['feature']['properties']['capit_porto'])

                subject = 'Nuova segnalazione [Da validare]'

            elif state_passage in ('2DV_1IL',):

                # Get every reporter by capit_porto
                users = AuthGroup.objects.get(name=settings.SIIM_USERS_GROUP['ccp_reporter']). \
                    user_set.filter(siimuserextradata__cp=kwargs['data']['feature']['properties']['capit_porto'])

                subject = 'Segnalazione [In Lavorazione]'

            elif state_passage in ('2DV_2PR', '2DV_2PU'):

                # Get every examiner
                users = AuthGroup.objects.get(name=settings.SIIM_USERS_GROUP['ccp_examiner']). \
                    user_set.all()

                subject = f'Segnalazione [{state_to.desc}]'

            elif state_passage in ('2PR_3IE', '2PU_3IE'):

                # Get every responsible
                users = AuthGroup.objects.get(name=settings.SIIM_USERS_GROUP['ccp_responsible']). \
                    user_set.all()

                subject = f'Segnalazione [{state_to.desc}]'

            elif state_passage in ('3IE_3RINC', ):

                # Get every reporter by capit_porto
                users = AuthGroup.objects.get(name=settings.SIIM_USERS_GROUP['ccp_reporter']). \
                    user_set.filter(siimuserextradata__cp=kwargs['data']['feature']['properties']['capit_porto'])

                subject = f'Segnalazione [{state_to.desc}]'

            if users:
                to = [u.email for u in users]

            if to and not kwargs['data']['feature']['properties'][SIIM_SIGNAL_TYPES[signal_type]['father_field']]:
                send_mail(
                    subject,
                    '',
                    'signaler_iim@g3wsuite.it',
                    to,
                    html_message=body_msg,
                    fail_silently=False,
                )


        except TemplateDoesNotExist:
            pass

        except Exception as e:
            logger.error(f'Error in sending email for type {signal_type} on passage state {state_passage}')






