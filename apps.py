from django.apps import AppConfig


class EditingIIMConfig(AppConfig):

    name = 'editing_iim'
    verbose_name = 'Editing IIM'

    def ready(self):

        # import signal handlers
        import editing_iim.receivers

        # Check for editing_imm project, if is not set raise exception
        from django.conf import settings
        if 'signaler_iim' not in settings.INSTALLED_APPS:
            raise Exception('\'editing_iim\' required \'signaler_iim\' module')
