# coding=utf-8
"""" Test for send email

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-04-15'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'

from django.conf import settings
from django.test import TestCase, override_settings
from django.core.mail import send_mail


@override_settings(
    #EMAIL_HOST='smtp.planetek.it',
    EMAIL_HOST='localhost',
    EMAIL_PORT=1025,
    EMAIL_USE_TLS=False

)
class SIIMEmailTest(TestCase):

    databases = {}

    def test_send_email(self):
        """ test send email """

        print(settings.EMAIL_HOST)

        sent = send_mail(
                'Testing send email',
                '',
                'signaler_iim@g3wsuite.it',
                ['lorenzetti@gis3w.it'],
                html_message='Body message',
                fail_silently=False,
            )

        self.assertEqual(sent, 1)

        send_mail(
            'Subject here',
            'Here is the message.',
            'from@example.com',
            ['lorenzetti@gis3w.it'],
            fail_silently=False,
        )
