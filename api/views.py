from django.views.decorators.csrf import csrf_exempt
from signaler_iim.utils import get_signal_type
from signaler_iim.api.filters import SignalByUserFilterBackend
from editing.api.views import QGISEditingLayerVectorView


class SIIMEditingLayerVectorView(QGISEditingLayerVectorView):

    filter_backends = QGISEditingLayerVectorView.filter_backends + (
        SignalByUserFilterBackend,
    )

    def dispatch(self, request, *args, **kwargs):

        # set with siim signal type
        self.signal_type = get_signal_type(kwargs['project_id'])
        return super().dispatch(request, *args, **kwargs)

    def _send_email(self):
        """ Send a mail after signal change state"""
        pass





@csrf_exempt  # put exempt here because as_view method is outside url bootstrap declaration
def layer_commit_vector_view(request, project_type, project_id, layer_name, *args, **kwargs):

    # instance module vector view
    view = SIIMEditingLayerVectorView.as_view()
    kwargs.update({'project_type': project_type, 'project_id': project_id, 'layer_name': layer_name})
    return view(request, *args, **kwargs)


